// file encoding: LATIN1
// actually using UTF8 seens to not work in Ubuntu 10.04 using gjs 0.5-1ubuntu2.3

const Gtk = imports.gi.Gtk;
const Glib = imports.gi.GLib;

// modificamos gtk-e1.js para añadir un label

//print('hello');

Gtk.init( null, null );

// inicializar el objeto
let win = new Gtk.Window({ type: Gtk.WindowType.TOPLEVEL });
let label = new Gtk.Label({ label: "En un lugar de la mancha de cuyo nombre no quiero acordarme\nHace tiempo ya\n vivía un hidalgo de esos que comen más papa que cordero ...\n\nDON QUIJOTE\n- Miguel de Cervantes Saavedra -" });

// set the window title
win.title = "Hola wey, js + GTK rocks!!!"
win.connect("destroy", function(){
	print("El usuario quiere acabar la app, bye!!!");
	Gtk.main_quit();
});

// add widgets to window
win.add(label);

// show widgets
label.show();
win.show();

// start the main thread
Gtk.main();